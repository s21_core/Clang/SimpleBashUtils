#include "s21_grep.h"

#include <getopt.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
  int window_pounce = 0;  // exit -1

  char **patterns = NULL;
  size_t patterns_sz = 0;

  char **files = NULL;
  size_t files_sz = 0;

  int fFlag = 0;
  int eFlag = 0;
  unsigned char flag = 0;

  if (argc < 3)
    window_pounce = 1;
  else
    flag = getOptions(&patterns, &patterns_sz, &files, &files_sz, argc, argv,
                      &fFlag, &eFlag, &window_pounce);

  if (!window_pounce) {  // if some opts unrecognised
    char *pattern_cat = catPatterns(patterns, patterns_sz);
    for (int i = 0; i < (int)files_sz; i++) {
      regex_file(files[i], pattern_cat, files_sz, flag);
    }
    free(pattern_cat);
  }

  destroy_stringArr(patterns, patterns_sz);
  destroy_stringArr(files, files_sz);

  if (window_pounce) {
    print_error();
  }
  return (window_pounce) ? -1 : 0;
}
// void print_bin(unsigned char n){
//   while (n) {
//     if (n & 1)
//         printf("1");
//     else
//         printf("0");

//     n >>= 1;
// }
// }
unsigned char getOptions(char ***patterns, size_t *patterns_sz, char ***files,
                         size_t *files_sz, int argc, char *argv[], int *fFlag,
                         int *sFlag, int *pounce) {
  unsigned char flag = 0;

  opterr = 0;
  while (optind < argc) {
    int opt = 0;
    if ((opt = getopt(argc, argv, "e:ivclnhsf:o")) != -1) {
      switch (opt) {
        case 'e':
          *sFlag = 1;
          addStrToArr(patterns, patterns_sz, optarg);
          break;
        case 'i':
          flag |= FLAG_I;
          break;
        case 'v':
          flag |= FLAG_V;
          break;
        case 'c':
          flag |= FLAG_C;
          break;
        case 'l':
          flag |= FLAG_L;
          break;
        case 'n':
          flag |= FLAG_N;
          break;
        case 'h':
          flag |= FLAG_H;
          break;
        case 's':
          flag |= FLAG_S;
          break;
        case 'f':
          if (add_patterns_f(patterns, patterns_sz, optarg) == 0) *pounce = 1;
          *fFlag = 1;
          break;
        case 'o':
          flag |= 1;
          break;
        case '?':
          if (optopt == 'e')
            fprintf(stderr, "grep: Option -%c requires an argument.\n", optopt);
          else if (optopt > 31 && optopt < 127)
            fprintf(stderr, "grep: invalid option `-%c'.\n", optopt);
          else
            fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
          *pounce = 1;
          break;
      }
    } else {
      addStrToArr(files, files_sz, argv[optind++]);
    }
  }
  if (*patterns_sz == 0 && *files_sz > 0) {  // no opts for patterns
    addStrToArr(patterns, patterns_sz, *files[0]);
    char *tmp = (*files)[0];

    for (int i = 1; i < (int)(*files_sz); i++) (*files)[i - 1] = (*files)[i];
    (*files_sz)--;
    free(tmp);
  }
  if (*patterns_sz == 0 || *files_sz == 0) *pounce = 1;
  if ((flag & FLAG_L) == FLAG_L)  // l flag ignores n
    flag &= (255 - FLAG_N);
  if ((flag & FLAG_V) == FLAG_V)  // v flag ignores o
    flag &= (255 - FLAG_O);
  return flag;
}

void addStrToArr(char ***arr, size_t *arr_sz, char *str) {
  (*arr_sz)++;
  *arr = (char **)realloc(*arr, sizeof(char *) * (*arr_sz));
  (*arr)[(*arr_sz) - 1] = malloc(sizeof(char) * (strlen(str) + 1));
  strcpy((*arr)[(*arr_sz) - 1], str);
}

int add_patterns_f(char ***patterns, size_t *patterns_sz, char *filename) {
  int result = 1;
  FILE *file = fopen(filename, "r");
  if (file) {
    char buf[4096];
    while (!feof(file)) {
      if (fgets(buf, 4096, file)) {
        if (buf[0] != '\n' && buf[strlen(buf) - 1] == '\n')
          buf[strlen(buf) - 1] = '\0';  // if empty line '\n' is pattern
        addStrToArr(patterns, patterns_sz, buf);
      }
    }
    fclose(file);
  } else {
    fprintf(stderr, "grep: %s: No such file or directory\n", filename);
    result = 0;
  }
  return result;
}

void destroy_stringArr(char **arr, size_t arr_sz) {
  for (int i = 0; i < (int)arr_sz; i++) free(arr[i]);
  free(arr);
}

int regex_str(char *string, char *pattern, unsigned char flags, char *res) {
  int ret = 0;
  int res_len = 0;
  regex_t preg;
  int cflags = REG_EXTENDED | REG_NEWLINE;
  int reti;
  size_t nmatch = 1;
  regmatch_t pmatch[1];
  if ((flags & FLAG_I) == FLAG_I) cflags |= REG_ICASE;
  reti = regcomp(&preg, pattern, cflags);
  if (reti) {
    fprintf(stderr, "Could not compile preg\n");
    return 0;
  }
  char *string_end = string + strlen(string);
  int res_o = 0;
  do {
    reti = regexec(&preg, string, nmatch, pmatch, 0);
    if (!reti) {
      ret = 1;
      if (((flags & FLAG_O) == FLAG_O)) {
        res_o = 1;
        while (pmatch->rm_so < pmatch->rm_eo)
          res[res_len++] = string[(pmatch->rm_so)++];
        res[res_len++] = '\n';
        res[res_len] = '\0';
        string = string + (int)pmatch->rm_eo;
      }
    } else if (reti == REG_NOMATCH) {
      ret = 0;
    } else {
      fprintf(stderr, "Regex match failed:\n");
      ret = 0;
      ;
    }
  } while (((flags & FLAG_O) == FLAG_O) && ret && string < string_end);
  if ((flags & FLAG_O) == FLAG_O) ret = res_o;
  regfree(&preg);
  return ((flags & FLAG_V) == FLAG_V) ? !ret : ret;
}

int regex_file(char *filename, char *pattern, size_t files_sz,
               unsigned char flag) {
  FILE *file = fopen(filename, "r");
  if (file) {
    int line_i = 1;
    int lines_count = 0;
    char buf[4096];
    char matches[4096];

    int stop = 0;
    while (!feof(file) && (!stop)) {
      if (fgets(buf, 4096, file))
        if (regex_str(buf, pattern, flag, matches)) {
          if (((flag & FLAG_C) == FLAG_C) || (flag & FLAG_L) == FLAG_L) {
            lines_count++;
          }
          if ((flag & FLAG_L) == FLAG_L)
            stop = 1;
          else
            print_line(buf, filename, files_sz, flag, line_i, matches);
        }
      line_i++;
    }
    if ((flag & FLAG_C) == FLAG_C) {  // print_summ_file();
      if (((int)files_sz > 1) && ((flag & FLAG_H) == 0))
        printf("%s:", filename);
      printf("%d\n", lines_count);
    }
    if ((flag & FLAG_L) == FLAG_L && lines_count) printf("%s\n", filename);

    fclose(file);
  } else {
    if ((flag & FLAG_S) != FLAG_S)
      fprintf(stderr, "grep: %s: No such file or directory\n", filename);
  }

  return 0;
}
// char *catPatterns(char **patterns, size_t patterns_sz) {
//   char *pattern_cat_prev = NULL, *pattern_cat_next = NULL;
//   int pattern_cat_sz = 0;
//   for (int i = 0; i < (int)patterns_sz; i++) {
//     pattern_cat_sz += strlen(patterns[i]) + 1;
//     pattern_cat_next = realloc(pattern_cat_prev = pattern_cat_next,
//                                sizeof(char) * (pattern_cat_sz + 1));
//     if (pattern_cat_next) {
//       pattern_cat_next = strcat(pattern_cat_next, patterns[i]);
//       pattern_cat_next[pattern_cat_sz - 1] = '|';
//     } else
//       free(pattern_cat_prev);
//   }
//   if (pattern_cat_next) pattern_cat_next[pattern_cat_sz - 1] = '\0';
//   return pattern_cat_next;
// }

char *catPatterns(char **patterns, size_t patterns_sz) {
  char *pattern_cat = NULL;
  int pattern_cat_sz = 0;
  for (size_t i = 0; i < patterns_sz; i++) {
    pattern_cat_sz += 1 + strlen(patterns[i]);
  }
  char *hernya = "|";
  pattern_cat = (char *)calloc(pattern_cat_sz, sizeof(char));
  for (int i = 0; i < (int)patterns_sz; i++) {
    pattern_cat = strcat(pattern_cat, patterns[i]);
    if (i != (int)patterns_sz - 1) pattern_cat = strcat(pattern_cat, hernya);
  }

  return pattern_cat;
}

void print_line(char *buf, char *filename, size_t files_sz, unsigned char flag,
                int line_number, char *matches) {
  if (((flag & FLAG_C) == FLAG_C) || ((flag & FLAG_L) == FLAG_L)) return;
  if (((int)files_sz > 1) && ((flag & FLAG_H) == 0)) printf("%s:", filename);
  if ((flag & FLAG_N) == FLAG_N) printf("%d:", line_number);
  if ((flag & FLAG_O) == FLAG_O)
    printf("%s", matches);
  else {
    printf("%s", buf);
    if (buf[strlen(buf) - 1] != '\n') printf("\n");
  }
}

void print_error() { fprintf(stderr, "Shit happens ¯\\_(ツ)_/¯ Bye-bye\n"); }
#ifndef S21_GREP
#define S21_GREP
#include <stdio.h>
#include <stdlib.h>
#define FLAG_O 1
#define FLAG_H 2
#define FLAG_N 4
#define FLAG_L 8
#define FLAG_C 16
#define FLAG_V 32
#define FLAG_I 64
#define FLAG_S 128

unsigned char getOptions(char ***patterns, size_t *patterns_sz, char ***files,
                         size_t *files_sz, int argc, char *argv[], int *fFlag,
                         int *sFlag, int *pounce);

void addStrToArr(char ***arr, size_t *arr_sz, char *str);

int add_patterns_f(char ***patterns, size_t *patterns_sz, char *filename);

void destroy_stringArr(char **patterns, size_t patterns_sz);

int regex_str(char *string, char *pattern, unsigned char flags, char *res);

int regex_file(char *filename, char *pattern, size_t files_sz,
               unsigned char flag);

char *catPatterns(char **patterns, size_t patterns_sz);

void print_line(char *buf, char *filename, size_t files_sz, unsigned char flag,
                int line_number, char *matches);

void print_error();

#endif
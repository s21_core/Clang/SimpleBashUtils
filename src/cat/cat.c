#include "cat.h"

#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
  int arg_st = 1;
  int cmd_flag = 0;

  if (argc > 1) {             // if there is some CMD options
    if (argv[1][0] == '-') {  // if CMD option 1 is some flag
      arg_st = 2;
      cmd_flag = getCmdKey(argv[1]);
    }
    if (cmd_flag > -1)
      for (; arg_st < argc; arg_st++) {
        FILE *file = fopen(argv[arg_st], "r");
        if (file) {
          if (cmd_flag == 0)
            simple_cat(file);
          else if (cmd_flag == 1 || cmd_flag == 2)
            cat_count_lines(file, cmd_flag);
          else if (cmd_flag == 3)
            cat_squeeze(file);
          else if (cmd_flag == 4 || cmd_flag == 5 || cmd_flag == 8 ||
                   cmd_flag == 10)
            cat_replacement(file, cmd_flag);

          fclose(file);
        } else {
          fprintf(stderr, "cat: %s: No such file or directory\n", argv[arg_st]);
        }
      }

    else {
      fprintf(stderr,
              "cat: illegal option -- %c\nusage: cat [-benstuv] [file ...]\n",
              argv[1][1]);
    }
  }
}

void simple_cat(FILE *file) {
  char ch;
  while ((ch = fgetc(file)) != EOF) {
    printf("%c", ch);
  }
}

int cat_replacement(FILE *file, int flag) {
  int sym = 0;

  char orig_HT[256][5];

  for (int i = 0; i < 256; i++) {
    orig_HT[i][0] = i;
    orig_HT[i][1] = '\0';
  }

  char *replacement_HT[] = {
      "^@",   "^A",   "^B",   "^C",    "^D",   "^E",   "^F",   "^G",   "^H",
      "^I",   "$\n",  "^K",   "^L",    "^M",   "^N",   "^O",   "^P",   "^Q",
      "^R",   "^S",   "^T",   "^U",    "^V",   "^W",   "^X",   "^Y",   "^Z",
      "^[",   "^\\",  "^]",   "^^",    "^_",   " ",    "!",    "\"",   "#",
      "$",    "%",    "&",    "\'",    "(",    ")",    "*",    "+",    ",",
      "-",    ".",    "/",    "0",     "1",    "2",    "3",    "4",    "5",
      "6",    "7",    "8",    "9",     ":",    ";",    "<",    "=",    ">",
      "?",    "@",    "A",    "B",     "C",    "D",    "E",    "F",    "G",
      "H",    "I",    "J",    "K",     "L",    "M",    "N",    "O",    "P",
      "Q",    "R",    "S",    "T",     "U",    "V",    "W",    "X",    "Y",
      "Z",    "[",    "\\",   "]",     "^",    "_",    "`",    "a",    "b",
      "c",    "d",    "e",    "f",     "g",    "h",    "i",    "j",    "k",
      "l",    "m",    "n",    "o",     "p",    "q",    "r",    "s",    "t",
      "u",    "v",    "w",    "x",     "y",    "z",    "{",    "|",    "}",
      "~",    "^?",   "M-^@", "M-^A",  "M-^B", "M-^C", "M-^D", "M-^E", "M-^F",
      "M-^G", "M-^H", "M-^I", "M-^J",  "M-^K", "M-^L", "M-^M", "M-^N", "M-^O",
      "M-^P", "M-^Q", "M-^R", "M-^S",  "M-^T", "M-^U", "M-^V", "M-^W", "M-^X",
      "M-^Y", "M-^Z", "M-^[", "M-^\\", "M-^]", "M-^^", "M-^_", "M- ",  "M-!",
      "M-\"", "M-#",  "M-$",  "M-%",   "M-&",  "M-'",  "M-(",  "M-)",  "M-*",
      "M-+",  "M-,",  "M--",  "M-.",   "M-/",  "M-0",  "M-1",  "M-2",  "M-3",
      "M-4",  "M-5",  "M-6",  "M-7",   "M-8",  "M-9",  "M-:",  "M-;",  "M-<",
      "M-=",  "M->",  "M-?",  "M-@",   "M-A",  "M-B",  "M-C",  "M-D",  "M-E",
      "M-F",  "M-G",  "M-H",  "M-I",   "M-J",  "M-K",  "M-L",  "M-M",  "M-N",
      "M-O",  "M-P",  "M-Q",  "M-R",   "M-S",  "M-T",  "M-U",  "M-V",  "M-W",
      "M-X",  "M-Y",  "M-Z",  "M-[",   "M-\\", "M-]",  "M-^",  "M-_",  "M-`",
      "M-a",  "M-b",  "M-c",  "M-d",   "M-e",  "M-f",  "M-g",  "M-h",  "M-i",
      "M-j",  "M-k",  "M-l",  "M-m",   "M-n",  "M-o",  "M-p",  "M-q",  "M-r",
      "M-s",  "M-t",  "M-u",  "M-v",   "M-w",  "M-x",  "M-y",  "M-z",  "M-{",
      "M-|",  "M-}",  "M-~",  "M-^?"};

  if ((flag == 4) || (flag == 5)) {  // -e-4 -t-5
    for (int i = 0; i < 256; i++) {
      if ((i == 9 && flag == 4) || (i == 10 && flag == 5)) continue;
      strcpy(orig_HT[i], replacement_HT[i]);
    }

  } else if (flag == 8) {  // -E
    strcpy(orig_HT[10], replacement_HT[10]);
  } else if (flag == 10) {  // -T
    strcpy(orig_HT[9], replacement_HT[9]);
  }

  int ch;
  while (((ch = fgetc(file)) != EOF)) printf("%s", orig_HT[ch]);

  // if (sym > 0) printf("\n");

  return sym;
}

int cat_count_lines(FILE *file, int flag) {
  int status = 0;

  char buf[4096];
  int line_counter = 1;

  while (!feof(file)) {
    // int len = 0;
    if (fgets(buf, 4096, file)) {
      // if ((len = fgets_(buf, 4096, file))!=EOF) {
      if (flag == 2 && buf[0] == '\n')
        printf("\n");
      else {
        printf("%6d\t%s", line_counter, buf);
        // printf("%6d\t", line_counter);
        // for(int i =0; i < len; i++)
        //   fputc(buf[i],stdout);
        line_counter++;
      }
    }
  }

  return status;
}
// int fgets_(char *buf,int limit, FILE * file){
//   int lenght = 0;
//   char ch;
//   while(((ch = fgetc(file)) != EOF) && (lenght < limit - 2) && (ch != '\n')){
//     buf[lenght++] = ch;
//   }
//   if (ch == '\n')
//     buf[lenght++] = ch;
//   buf[lenght] = '\0'; //just in case
//   if (lenght == 0 && ch == EOF)
//     lenght = EOF;

//   return lenght;
// }

int cat_squeeze(FILE *file) {
  int status = 0;

  int is_prev_line_empty = 0;
  char buf[4096];

  while (!feof(file)) {
    if (fgets(buf, 4096, file)) {
      if (buf[0] == '\n') {
        if (!is_prev_line_empty) {
          printf("\n");
        }
        is_prev_line_empty = 1;
      } else {
        printf("%s", buf);
        is_prev_line_empty = 0;
      }
    }
  }

  return status;
}

int getCmdKey(char *flag_str) {
  int flag = -1;
  // printf("GetCMD flag %c\n", argv[1][1]);
  switch (flag_str[1]) {
    case 'e':
      flag = 4;
      // printf("e");
      break;

    case 'b':
      flag = 2;
      break;

    case 'n':
      flag = 1;
      break;

    case 's':
      flag = 3;
      break;

    case 't':
      flag = 5;
      break;

    case 'v':
      flag = 6;
      break;

    case 'E':
      flag = 8;
      break;

    case 'T':
      flag = 10;
      break;

    case '-':
      flag = gnu(flag_str);

      break;
  }
  return flag;
}

int gnu(char *command) {
  int flag = -1;
  if (strcmp(command, "--number-nonblank") == 0) {
    flag = 2;
  } else if (strcmp(command, "--number") == 0) {
    flag = 1;
  } else if (strcmp(command, "--squeeze-blank") == 0) {
    flag = 3;
  }

  return flag;
}
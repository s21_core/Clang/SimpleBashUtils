#ifndef S21_CAT
#define S21_CAT

#include <stdio.h>
#include <string.h>

int gnu(char *command);
void simple_cat(FILE *file);
int cat_replacement(FILE *file, int flag);
int cat_count_lines(FILE *file, int flag);
// int fgets_(char *buf, int limit, FILE *file);
int cat_squeeze(FILE *file);
int getCmdKey(char *flag_str);
#endif